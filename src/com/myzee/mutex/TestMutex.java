package com.myzee.mutex;

import java.util.concurrent.Semaphore;

public class TestMutex {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Semaphore sem = new Semaphore(1);
		// TODO Auto-generated method stub
		Thread t1 = new Thread(new MyThread(sem));
		t1.start();
		
		Thread t2 = new Thread(new MyThread(sem));
		t2.start();
		
		Thread t3 = new Thread(new MyThread(sem));
		t3.start();
		
		Thread t4 = new Thread(new MyThread(sem));
		t4.start();
		
		Thread t5 = new Thread(new MyThread(sem));
		t5.start();
		
		Thread t6 = new Thread(new MyThread(sem));
		t6.start();
		
	}
}

class MyThread implements Runnable {
	Semaphore sem ;
	public MyThread(Semaphore sem) {
		// TODO Auto-generated constructor stub
		this.sem = sem;
	}
	public void run() {
		try {
			sem.acquire();
			System.out.println("lock aquired for " + Thread.currentThread().getName());
			Thread.sleep(1000);
			sem.release();
			System.out.println("lock released for " + Thread.currentThread().getName());
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}